<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class DatosController extends Controller
{

    public function __invoke(){

        return "Mensaje impreso pasando por el controlador";


    }

    public function imprimir(){

        return view('proyectos');


    }
    public function show($datos){

        return view('index',[ 'dato'=>$datos]);


    }
    public function index($datos){

        if ($datos) {
            return view('index',[ 'dato'=>$datos]);
        }else{
            return view('proyectos');
        }

        


    }


}
